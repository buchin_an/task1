package com.company;

public class Main {

    public static void main(String[] args) {

        final int EXPONENT = 2;

        int firstValue = 2;
        int secondValue = 1;
        int thirdValue = 3;


        if (firstValue < secondValue && firstValue < thirdValue) {
            System.out.println(Math.pow(secondValue, EXPONENT) + Math.pow(thirdValue, EXPONENT));
        }
        if (secondValue < firstValue && secondValue < thirdValue) {
            System.out.println(Math.pow(firstValue, EXPONENT) + Math.pow(thirdValue, EXPONENT));
        }
        if (thirdValue < firstValue && thirdValue < secondValue) {
            System.out.println(Math.pow(firstValue, EXPONENT) + Math.pow(secondValue, EXPONENT));
        }

        // можно решить не используя if()

        System.out.println(Math.pow(firstValue, EXPONENT)
                + Math.pow(secondValue, EXPONENT)
                + Math.pow(thirdValue, EXPONENT)
                - Math.pow((Math.min(Math.min(firstValue, secondValue), thirdValue)), EXPONENT));
    }
}
